import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by dcatalans on 23/05/16.
 */
class Tar {
    private final String file_name;
    private ArrayList<Archive> l_archive;


    // Constructor
    public Tar(String filename) {
        this.file_name = filename;
    }

    public static void main(String[] args) throws IOException {
        Scanner s = new Scanner(System.in);

        Tar t = null;
        while (true) {
            System.out.println("Welcome to shell, please make an order: ");
            String userInput = s.nextLine();

            String[] array_choice = userInput.split(" +");
            String order = array_choice[0];

            if (order.toLowerCase().equals("load")) {
                try {
                    String nom_fitxer = array_choice[1];

                    File f = new File(nom_fitxer);
                    if (!f.exists()) {
                        System.out.println("El fitxer no existeix");
                    } else {
                        t = new Tar(nom_fitxer);
                        t.expand();
                        System.out.println("File loaded correctly.");
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Not files found");
                }
            } else if (order.toLowerCase().equals("list")) {
                try {
                    System.out.println(Arrays.toString(t.list()));
                } catch (NullPointerException e) {
                    System.out.println("You should load a Tar File first.");
                }

            } else if (order.toLowerCase().equals("extract")) {
                try {
                    if (t == null) {
                        System.out.println("You should load a Tar File first.");
                        continue;
                    }
                    if (array_choice.length < 3) {
                        System.out.println("Not enough parametres: <extract file or files destinationfolder>");
                        continue;
                    }
                    for (int i = 1; i < array_choice.length - 1; i++) {
                        String nom_fitxer = array_choice[i];
                        String dest;


                        if (array_choice[array_choice.length - 1].equals('/')) {
                            dest = array_choice[array_choice.length - 1] + nom_fitxer;
                        } else {
                            dest = array_choice[array_choice.length - 1] + "/" + nom_fitxer;
                        }

                        FileOutputStream fos = new FileOutputStream(dest);
                        fos.write(t.getBytes(nom_fitxer));
                        fos.close();
                        System.out.println("Files Extracted Correctly");
                    }

                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("You should load a Tar File first.");
                } catch (FileNotFoundException e) {
                    System.out.println("Destination folder doesn't exists");
                }
            } else if (order.toLowerCase().equals("exit")) {
                break;
            }
        }

    }

    // Torna un array amb la llista de fitxers que hi ha dins el TAR
    public String[] list() {
        String[] l_array = new String[l_archive.size()];
        for (int i = 0; i < l_archive.size(); i++) {
            l_array[i] = l_archive.get(i).getFile_name();
        }
        return l_array;
    }

    // Torna un array de bytes amb el contingut del fitxer que té per nom
    // igual a l'String «name» que passem per paràmetre
    public byte[] getBytes(String name) {
        for (Archive aL_archive : l_archive) {
            if (name.equals(aL_archive.getFile_name())) {
                return aL_archive.getContingut();
            }
        }
        return null;
    }

    // Expandeix el fitxer TAR dins la memòria
    public void expand() {
        try {
            FileInputStream is = new FileInputStream(this.file_name);
            int i;
            //Variable per emmagatzemar el nom de cada arxiu

            l_archive = new ArrayList<>();

            while (true) {
                String infile_name = "";
                String insize = "";

                for (int j = 0; j < 100; j++) {
                    i = is.read();
                    if (i > 0) infile_name += (char) i;
                }
                if (infile_name.equals("")) break;

                for (int j = 0; j < 24; j++) {
                    is.read();
                }

                for (int j = 0; j < 11; j++) {
                    i = is.read();
                    insize += (char) i;
                }
                is.read();
                int size = Integer.parseInt(insize, 8);

                // avancem fins al començament del fitxer
                is.skip(376);

                ByteArrayOutputStream contingut = new ByteArrayOutputStream();
                for (int j = 0; j < size; j++) {
                    contingut.write(is.read());
                }
                byte[] bytes = contingut.toByteArray();

                int avanzar = 512 - (size % 512);
                is.skip(avanzar);
                l_archive.add(new Archive(infile_name, size, bytes));
            }

        } catch (FileNotFoundException e) {
            System.out.println("No ha introiduït un arxiu valid, per favor carregui l'arxiu de nou");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

class Archive {
    private int size;
    private String file_name;
    private byte[] contingut;

    public byte[] getContingut() {
        return contingut;
    }

    public String getFile_name() {
        return file_name;
    }

    public Archive(String nom, int size, byte[] contingut) {
        this.file_name = nom;
        this.size = size;
        this.contingut = contingut;
    }

    public String toString() {
        return file_name + " " + size + " " + contingut;
    }

}